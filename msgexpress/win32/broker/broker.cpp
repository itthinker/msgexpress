// broker.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include "spwin32iocp.hpp"
#include "spiocpserver.hpp"
#include "servicemanager.h"
#include "msghandler.h"

#pragma comment(lib,"ws2_32")
#pragma comment(lib,"mswsock")

//---------------------------------------------------------
using namespace google::protobuf;
int main( int argc, char * argv[] )
{
	int port = 23, maxThreads = 10;

	if( 0 != sp_initsock() ) assert( 0 );

	ServiceManager manager;

	SP_IocpServer server( "", port, new MsgHandlerFactory( &manager ) );
	server.setTimeout( 600 );
	server.setMaxThreads( maxThreads );
	server.setMaxConnections(4096);
	server.setReqQueueSize( 100, "Sorry, server is busy now!\n" );

	server.runForever();

	sp_closelog();

	return 0;
}


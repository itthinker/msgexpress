
#include <winsock2.h>
#include <windows.h>
#include <stdio.h>
#include <assert.h>

#include <signal.h>
#include <stdio.h>
#include <sys/types.h>
#include <stdlib.h>
#include <errno.h>
#include <time.h>
#include <string.h>

#include "spthread.hpp"

#include "spporting.hpp"
#include "spbuffer.hpp"
#include "spthreadpool.hpp"
#include <spwin32port.hpp>
#include "protobuf.h"
#include "messageutil.h"
#include "package.h"
#include "msgdecoder.h"

#include "test.pb.h"
#include "msgexpress.h"
#include "spclient.h"


#pragma comment(lib,"ws2_32")
#pragma comment(lib,"mswsock")
#pragma comment(lib,"advapi32")
#pragma comment(lib,"libprotobuf.lib")






static struct SP_TestStat gStat;
static time_t gStartTime = 0;
//
//static const char * gHost = "192.168.1.176";
//static int gPort = 23;

static const char * gHost = "127.0.0.1";
static int gPort = 23;

//static const char * gHost = "192.168.1.242";
//static int gPort = 6875;


void showUsage( const char * program )
{
	printf( "Stress Test Tools for spserver example\n" );
	printf( "Usage: %s [-h <host>] [-p <port>] [-c <clients>] [-m <messages>]\n", program );
	printf( "\t-h default is %s\n", gHost );
	printf( "\t-p default is %d\n", gPort );
	printf( "\n" );
}

void close_client( SP_TestClient * client )
{
	if( 0 == client->mIsStop ) {
		client->mIsStop = 1;
	}
}


SP_Buffer databuff;
MsgDecoder decoder;

void sendmsg( SP_TestClient * client,Message* msg,unsigned int topic )
{
	static int serial=1;
	std::string data;
	MessageUtil::serializePackageToString(*msg,PackageHeader::Request,topic,serial++,0,&data);

	int bytesTransferred = send( (SOCKET)client->mFd, data.c_str(), data.size(), 0 );
	if( bytesTransferred <= 0 ) {
		if( bytesTransferred < 0 ) {
			printf( "send fail, errno %d\n", WSAGetLastError() );
			gStat.mSendFail++;
		}
		close_client( client );
		return;
	}
	else if(bytesTransferred<data.size())
	{
		printf( "lost data, errno \n");
	}
}
void reply( SP_TestClient * client,Package* package,Message* msg)
{
	static int serial=1;
	std::string data;
	MessageUtil::serializePackageToString(*msg,PackageHeader::Response,package->getCommand(),serial++,package->getSrcAddr(),&data);

	int bytesTransferred = send( (SOCKET)client->mFd, data.c_str(), data.size(), 0 );
	if( bytesTransferred <= 0 ) {
		if( bytesTransferred < 0 ) {
			printf( "send fail, errno %d\n", WSAGetLastError() );
			gStat.mSendFail++;
		}
		close_client( client );
		return;
	}
}
void publish( SP_TestClient * client, MsgExpress::PublishData* msg )
{
	static int serial=1;
	std::string data;
	MessageUtil::serializePackageToString(*msg,PackageHeader::Publish,0,serial++,0,&data);

	int bytesTransferred = send( (SOCKET)client->mFd, data.c_str(), data.size(), 0 );
	if( bytesTransferred <= 0 ) {
		if( bytesTransferred < 0 ) {
			printf( "send fail, errno %d\n", WSAGetLastError() );
			gStat.mSendFail++;
		}
		close_client( client );
		return;
	}
}
void onRequest(MessagePtr request,Message*& response)
{
	if(0==strcmp(request->GetTypeName().c_str(),Test::Faq::default_instance().GetTypeName().c_str() ))
	{
		Test::Faq* faq=(Test::Faq*)request.get();
		//printf("request:%s\r\n",faq->ShortDebugString().c_str());
		time_t timer; 
		time(&timer); 
		response=new Test::Faq(*faq);
		((Test::Faq*)response)->set_answer(ctime(&timer));
	}
	else if(0==strcmp(request->GetTypeName().c_str(),Test::Hello::default_instance().GetTypeName().c_str() ))
	{
		response=new Test::Hello(*(Test::Hello*)request.get());
	}
}
void onPublish(MessagePtr pub)
{
	MsgExpress::PublishData* pubData=(MsgExpress::PublishData*)pub.get();
	//int subId=pubData->id();
	printf("Publish:%s\r\n",pub->ShortDebugString().c_str());
}
void on_read( SP_TestClient * client, SP_TestEvent * event )
{
	char buffer[ 4096 ] = { 0 };
	int bytesTransferred = recv( (int)client->mFd, buffer, sizeof( buffer ), 0 );

	if( bytesTransferred <= 0 ) {
		if( bytesTransferred < 0 ) {
			printf( "recv fail, errno %d\n", WSAGetLastError() );
			gStat.mRecvFail++;
		}
		return;
	}

	databuff.append(buffer,bytesTransferred);
	int ret=decoder.decode(&databuff);
	if(ret==SP_MsgDecoder::eOK)
	{
		
	}
	memset( &( event->mOverlapped ), 0, sizeof( OVERLAPPED ) );
	event->mType = SP_TestEvent::eEventRecv;
	event->mWsaBuf.buf = NULL;
	event->mWsaBuf.len = 0;

	DWORD recvBytes = 0, flags = 0;
	if( SOCKET_ERROR == WSARecv( (SOCKET)client->mFd, &( event->mWsaBuf ), 1,
			&recvBytes, &flags, &( event->mOverlapped ), NULL ) ) {
		if( ERROR_IO_PENDING != WSAGetLastError() ) {
			gStat.mWSARecvFail++;
			printf( "WSARecv fail, errno %d\n", WSAGetLastError() );
			close_client( client );
		}
	}
}

void on_write( SP_TestClient * client, SP_TestEvent * event )
{
	DWORD sendBytes = 1;

	event->mType = SP_TestEvent::eEventSend;
	memset( &( event->mOverlapped ), 0, sizeof( OVERLAPPED ) );
	event->mWsaBuf.buf = NULL;
	event->mWsaBuf.len = 0;
	if( SOCKET_ERROR == WSASend( (SOCKET)client->mFd, &( event->mWsaBuf ), 1,
			&sendBytes, 0,	&( event->mOverlapped ), NULL ) ) {
		if( ERROR_IO_PENDING != WSAGetLastError() ) {
			gStat.mWSASendFail++;
			printf( "WSASend fail, errno %d\n", WSAGetLastError() );
			close_client( client );
		}
	}
}
bool stop=false;
void processMsg(HANDLE param)
{
	SP_TestClient * client=(SP_TestClient *) param;
	while(!stop)
	{
		SP_BlockingQueue* queue=decoder.getQueue();
		Package* package=NULL;
		while(package=(Package*)queue->pop())
		{
			if(package->getMessage().get())
			{
				unsigned char type=package->getPackageType();
				if(type==PackageHeader::Publish)
				{
					onPublish(package->getMessage());
				}
				else if(type==PackageHeader::Response)
				{
					if(package->getSerialNum()% 100==0)
						printf("Response:%d\r\n",package->getSerialNum());
					//printf("Response:%s\r\n",package->getDebugString().c_str());
				}
				else if(type==PackageHeader::Request)
				{
					Message* resp=NULL;
					onRequest(package->getMessage(),resp);
					reply(client,package,resp);
				}
			}
		}
	}
}
void eventLoop( HANDLE hIocp )
{
	DWORD bytesTransferred = 0;
	SP_TestClient * client = NULL;
	SP_TestEvent * event = NULL;

	while(!stop)
	{
		BOOL isSuccess = GetQueuedCompletionStatus( hIocp, &bytesTransferred,
				(DWORD*)&client, (OVERLAPPED**)&event, 10000 );
		DWORD lastError = WSAGetLastError();

		if( ! isSuccess ) {
			if( NULL != client ) {
				gStat.mGQCSFail++;
				close_client( client );
			}
			continue;
		}
		//printf( "thread#%ld\n", sp_thread_self() );
		if( SP_TestEvent::eEventRecv == event->mType ) {
			on_read( client, event );
		}

		if( SP_TestEvent::eEventSend == event->mType ) {
			on_write( client, event );
		}
	}
}

namespace News
{
	const static int KEY_TITLE=1;
	const static int KEY_AUTHOR=2;
	const static int KEY_DATE=3;
	const static int KEY_CONTENT=4;

	const static int TOPIC_HELLOWORLD=0x00100005;
};
namespace Test
{
	const static int FUNC_FAQ=0x00100001;
	const static int FUNC_HELLO=0x0010000a;
};

int initial(HANDLE * hIocp_out, SP_TestClient** clientList_out, const char * host, const int port)
{
	WSADATA wsaData;
	static const int totalClients = 1;
	int err = WSAStartup( MAKEWORD( 2, 0 ), &wsaData );
	if ( err != 0 ) {
		printf( "Couldn't find a useable winsock.dll.\n" );
		return -1;
	}

	*hIocp_out = CreateIoCompletionPort( INVALID_HANDLE_VALUE, NULL, 0, 0 );
	if( NULL == hIocp_out ) {
		printf( "CreateIoCompletionPort failed, errno %d\n", WSAGetLastError() );
		return -1;
	}

	*clientList_out = (SP_TestClient*)calloc( totalClients, sizeof( SP_TestClient ) );

	struct sockaddr_in sin;
	memset( &sin, 0, sizeof(sin) );
	sin.sin_family = AF_INET;
	sin.sin_addr.s_addr = inet_addr( host );
	sin.sin_port = htons( port );

	printf( "Create %d connections to server, it will take some minutes to complete.\n", 1 );
	for( int i = 0; i < totalClients; i++ ) {
		SP_TestClient * client = *clientList_out + i;
		memset( client, 0, sizeof( SP_TestClient ) );

		client->mFd = socket( AF_INET, SOCK_STREAM, 0 );
		if( client->mFd < 0 ) {
			printf( "socket failed, errno %d, %s\n", errno, strerror( errno ) );
			spwin32_pause_console();
			return -1;
		}

		if( connect( client->mFd, (struct sockaddr *)&sin, sizeof(sin) ) != 0) {
			printf( "connect failed, errno %d, %s\n", errno, strerror( errno ) );
			spwin32_pause_console();
			return -1;
		}

		if( NULL == CreateIoCompletionPort( (HANDLE)client->mFd, *hIocp_out, (DWORD)client, 0 ) ) {
			printf( "CreateIoCompletionPort failed, errno %d\n", WSAGetLastError() );
			return -1;
		}

		if( 0 == ( i % 10 ) ) printf( "." );
	}

	for( int i = 0; i < totalClients; i++ ) {
		SP_TestClient * client = *clientList_out + i;
		//on_read(client,&(client->mRecvEvent));
		memset( &(client->mRecvEvent.mOverlapped), 0, sizeof(OVERLAPPED) );
		client->mRecvEvent.mType = SP_TestEvent::eEventRecv;
		client->mRecvEvent.mWsaBuf.buf = NULL;
		client->mRecvEvent.mWsaBuf.len = 0;

		DWORD recvBytes = 0, flags = 0;
		if( SOCKET_ERROR == WSARecv( (SOCKET)client->mFd, &( client->mRecvEvent.mWsaBuf ), 1,
				&recvBytes, &flags, &( client->mRecvEvent.mOverlapped ), NULL ) ) {
			if( ERROR_IO_PENDING != WSAGetLastError() ) {
				gStat.mWSARecvFail++;
				printf( "WSARecv fail, errno %d\n", WSAGetLastError() );
				close_client( client );
			}
		}
	}
	return 0;
}

void closeClients(SP_TestClient* const clientList,const int totalClients)
{
	int totalSend = 0, totalRecv = 0;
	for( int i = 0; i < totalClients; i++ ) {
		SP_TestClient * client = clientList + i;

		if( INVALID_HANDLE_VALUE != (HANDLE)client->mFd ) {
			closesocket( client->mFd );
		}
	}
	Sleep(1000);
	free( clientList );

}

int main( int argc, char * argv[] )
{
	HANDLE hIocp;
	SP_TestClient * clientList;
	int totalClients = 3;
	if(int r = initial(&hIocp, & clientList,gHost,gPort)){
		return r;
	}
	
	//建立处理消息的线程池
	SP_ThreadPool poolRead( 1 );
	poolRead.dispatch(eventLoop,hIocp);

	SP_ThreadPool poolProcess( 1 );
	poolProcess.dispatch(processMsg,clientList);
	//开始发消息

	printf("Please input:\r\n");
	printf("r : register service.\r\n");
	printf("s : subscribe.\r\n");
	printf("u : unsubscribe.\r\n");
	printf("p : publish data.\r\n");
	printf("g : send request.\r\n");
	printf("h : send request.\r\n");
	printf("t : repeat send request.\r\n");
	printf("q : quit.\r\n");
	while(1)
	{
		char c;
		scanf("%c",&c);
		if(c=='r')
		{
			MsgExpress::RegService req;
			req.add_functionid(Test::FUNC_FAQ);
			req.add_functionid(Test::FUNC_HELLO);
			printf("register service:%s\r\n",req.ShortDebugString().c_str());
			sendmsg(clientList,&req,MsgExpress::Cmd_RegService);
		}
		else if(c=='s')
		{
			MsgExpress::SubscribeData sub;
			sub.set_id(1);
			sub.set_topic(News::TOPIC_HELLOWORLD);
			MsgExpress::DataItem* item=sub.add_condition();
			item->set_key(News::KEY_AUTHOR);
			item->add_value("lee");

			printf("subscribe:%s\r\n",sub.ShortDebugString().c_str());
			sendmsg(clientList,&sub,MsgExpress::Cmd_Subscribe);
		}
		else if(c=='u')
		{
			MsgExpress::UnSubscribeData sub;
			sub.set_id(1);
			printf("unsubscribe:%s\r\n",sub.ShortDebugString().c_str());
			sendmsg(clientList,&sub,MsgExpress::Cmd_UnSubscribe);
		}
		else if(c=='p')
		{
			MsgExpress::PublishData news;
			news.set_topic(News::TOPIC_HELLOWORLD);
			MsgExpress::DataItem* item=news.add_item();
			item->set_key(News::KEY_TITLE);
			item->add_value("Hello world!");
			item=news.add_item();
			item->set_key(News::KEY_AUTHOR);
			static int s=0;
			s++;
			if(s%2==0)
			    item->add_value("lee");
			else 
				item->add_value("wang");
			item=news.add_item();
			item->set_key(News::KEY_DATE);
			item->add_value("2014-01-01");
			item=news.add_item();
			item->set_key(News::KEY_CONTENT);
			item->add_value("hello world,hello sumscope!");
			printf("publish data:%s\r\n",news.ShortDebugString().c_str());
			publish(clientList,&news);
		}
		else if(c=='g')
		{
			Test::Faq faq;
			faq.set_question("What time is it?");
			sendmsg(clientList,&faq,Test::FUNC_FAQ);
		}
		else if(c=='h')
		{
			Test::Hello hello;
			hello.set_content("Hello world");
			sendmsg(clientList,&hello,Test::FUNC_HELLO);
		}
		else if(c=='t')
		{
			Test::Faq faq;
			faq.set_question("What time is it?");
			for(int i=0;i<100000;i++)
			{
			    sendmsg(clientList,&faq,Test::FUNC_FAQ);
			}
			printf("over.\r\n");
		}
		else if(c=='q')
		{
			printf("quit.\r\n");
			stop=true;
			break;
		}
	}
	closeClients(clientList,totalClients);
	CloseHandle( hIocp );
	spwin32_pause_console();

	return 0;
}


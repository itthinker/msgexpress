#ifndef _MSGCLIENT_TEST_H_
#define _MSGCLIENT_TEST_H_

#include <winsock2.h>
#include <windows.h>
#include <stdio.h>
#include <assert.h>

#include <signal.h>
#include <stdio.h>
#include <sys/types.h>
#include <stdlib.h>
#include <errno.h>
#include <time.h>
#include <string.h>
#include <list>

#include "spthread.hpp"

#include "spporting.hpp"
#include "spbuffer.hpp"
#include "spthreadpool.hpp"
#include <spwin32port.hpp>
#include "protobuf.h"
#include "messageutil.h"
#include "package.h"
#include "msgdecoder.h"
#include "spclient.h"

#include "test.pb.h"
#include "msgexpress.h"

//
///@brief 客户端消息回调函数定义
typedef void (WINAPI *LPONMESSAGE)(Package msg);
///@brief 客户端事件回调函数定义
typedef void (WINAPI *LPONEVENT)(int eventid);
///@brief 客户端初始化函数,
extern "C" BOOL WINAPI InitializeClient();
///@brief 客户端释放函数,
extern "C" BOOL WINAPI ReleaseClient();
///@brief 注册一个回调函数
extern "C" BOOL WINAPI RegisterMsgCallBack(LPONMESSAGE pOnMessage);
///@brief 反注册一个用于接收某类消息的回调函数
extern "C" BOOL WINAPI UnRegisterMsgCallBack(LPONMESSAGE pOnMessage);
///@brief 注册一个回调函数
extern "C" BOOL WINAPI RegisterEventCallBack(LPONEVENT pOnMessage);
///@brief 反注册一个用于接收某类消息的回调函数 
extern "C" BOOL WINAPI UnRegisterEventCallBack(LPONEVENT pOnMessage);
///@brief 客户端发送异步消息的接口
extern "C" unsigned int WINAPI PostMessageApi(MessagePtr msg);
///@brief 客户端发送同步消息的接口
extern "C" BOOL WINAPI SendMessageApi(MessagePtr request, MessagePtr response,unsigned int unTimeOut);
//






class SP_Client {
private:
	char * host;
	int port;
	HANDLE  hIocp;
	SP_TestClient * client;
	list<LPONMESSAGE> messageList;
	list<LPONEVENT> eventList;
	static SP_Client * me;
	unsigned int count;  
	struct SP_TestStat gStat;
	SP_Buffer databuff;
	MsgDecoder decoder;

	SP_Client();
	~SP_Client();


	static void messageRead( HANDLE hIocp );
	void	on_read(DWORD bytesTransferred);


public:
	static SP_Client* Get_SP_Client();

	///@brief 客户端初始化函数,
	BOOL InitializeClient();
	///@brief 客户端释放函数,
	BOOL ReleaseClient();
	///@brief 注册一个回调函数
	BOOL RegisterMsgCallBack(LPONMESSAGE pOnMessage);
	///@brief 反注册一个用于接收某类消息的回调函数
	BOOL UnRegisterMsgCallBack(LPONMESSAGE pOnMessage);
	///@brief 注册一个回调函数
	BOOL RegisterEventCallBack(LPONEVENT pOnMessage);
	///@brief 反注册一个用于接收某类消息的回调函数
	BOOL UnRegisterEventCallBack(LPONEVENT pOnMessage);
	///@brief 客户端发送异步消息的接口
	unsigned int PostMessage(MessagePtr msg);
	///@brief 客户端发送同步消息的接口
	BOOL SendMessage(MessagePtr request, MessagePtr response,unsigned int unTimeOut);

	};





#endif